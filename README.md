ขั้นตอนการติดตั้งเครื่อง WebServer โดยใช้ XAMPP 
1. เริ่มต้นจากดาวน์โหลด XAMPP: ได้ที่ URL: https://www.apachefriends.org/index.html
2. ทำการติดตั้งระหว่างการติดตั้ง หากมีหน้าต่าง Windows Security Alert ขึ้นมาถามให้กดปุ่ม Allow access เพื่ออนุญาติในการเข้าถึง
3. ทำตามขั้นตอนจนถึงขั้นตอนสุดท้ายและกดปุ่ม Finish 
4. หน้าต่าง XAMPP Control Panel   จะปรากฏขึ้นมา  ให้กดปุ่ม Start เพียง 2 บรรทัดแรก คือ  Apache และ MySQL เท่านั้น กรณี กดปุ่ม Start บรรทัด MySQL  จะมีหน้าต่าง Windows Security Alert ขึ้นมาถามให้กดปุ่ม Allow access เพื่ออนุญาติในการเข้าถึง
5. เสร็จสิ้น
